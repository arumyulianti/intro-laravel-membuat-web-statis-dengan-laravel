<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register()
    {
    	return view('data.form');
    }

    public function form (Request $request)
    {
    	$first = $request->fname;
    	$last = $request->lname;
    	//dd($first,$last);

    	return view ('data.welcome', compact('first','last'));
    }
}
