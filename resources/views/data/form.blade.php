<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Blade </title>
  </head>
  <body>
    <form action="/welcome">
        <h1> Buat Account Baru ! </h1>
        <h3> Sign Up Form </h3>

        @csrf
        <p> First Name: </p>
        <input type="text" name="fname"> <br><br>
        
        <p> Last Name: </p>
        <input type="text" name="lname"> <br><br>
        
        <p> Gender </p>
        <input type="radio" name="gender_user"> Male <br>
        <input type="radio" name="gender_user"> Female <br>
        <input type="radio" name="gender_user"> Other <br><br>

        <p> Nationality </p>
        <select name="negara">
            <option value="indonesian"> Indonesian </option>
            <option value="singaporean"> Singaporean </option>
            <option value="malaysian"> Malaysian </option>
            <option value="australian"> Australian </option> 
        </select> <br><br>

        <p> Language Spoken: </p>
        <input type="checkbox" name="language_user" value="0"> Bahasa Indonesia <br>
        <input type="checkbox" name="language_user" value="1"> English <br>
        <input type="checkbox" name="language_user" value="2"> Other <br><br>

        <p> Bio: </p>
        <textarea cols="50" rows="15"> </textarea> <br><br>

        <input type="Submit" value="Sign Up" action="\welcome">
     </form>
  <body>
  </html>